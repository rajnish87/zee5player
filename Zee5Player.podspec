#
# Be sure to run `pod lib lint Zee5Player.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'Zee5Player'
  s.version          = '0.1.0'
  s.summary          = 'kjsdsa kjsdjsad kjshdsa ojasdhjksa kjashdjskahd'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

s.description      = 'klsajd a;lsdksa lssaddk ;plaskdsa l;askd l;asdsajdkjsdj ksjdskajd'

  s.homepage         = 'https://bitbucket.org/rajnish87/zee5player/src/master/'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'rajnish87' => 'rajnish.kumar@zee.esselgroup.com' }
  s.source           = { :git => 'https://rajnish87@bitbucket.org/rajnish87/zee5player.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'
 s.swift_version = '4.2'
  s.source_files = 'Zee5Player/Classes/**/*'
  
  # s.resource_bundles = {
  #   'Zee5Player' => ['Zee5Player/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
