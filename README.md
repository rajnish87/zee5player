# Zee5Player

[![CI Status](https://img.shields.io/travis/RajnishKumar1987/Zee5Player.svg?style=flat)](https://travis-ci.org/RajnishKumar1987/Zee5Player)
[![Version](https://img.shields.io/cocoapods/v/Zee5Player.svg?style=flat)](https://cocoapods.org/pods/Zee5Player)
[![License](https://img.shields.io/cocoapods/l/Zee5Player.svg?style=flat)](https://cocoapods.org/pods/Zee5Player)
[![Platform](https://img.shields.io/cocoapods/p/Zee5Player.svg?style=flat)](https://cocoapods.org/pods/Zee5Player)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

Zee5Player is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'Zee5Player'
```

## Author

RajnishKumar1987, rajnish.kumar@zee.esselgroup.com

## License

Zee5Player is available under the MIT license. See the LICENSE file for more info.
