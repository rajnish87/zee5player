//
//  Zee5Player.swift
//  Pods
//
//  Created by Rajnish Kumar on 10/05/19.
//

import Foundation

import UIKit
extension UIImageView {
    public func circleImageView(borderColor: UIColor, borderWidth: CGFloat){
        self.layer.borderColor = borderColor.cgColor
        self.layer.borderWidth = borderWidth
        self.layer.cornerRadius = self.layer.frame.size.width / 2
        self.clipsToBounds = true
    }
}
